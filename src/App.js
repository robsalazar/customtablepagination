import React, { Component } from 'react';
import ComponentsContainer from './ComponentsContainer/ComponentsContainer'
import './App.css';
import './stylesComp.sass'
const ButtonTest = () => (
  <button> Hi </button>
)
const selectedPagStyle = 'tableComp'
const paginationStyle = 'paginationStyle'
const searchBoxPlaceHolder = 'Search'
const tableStyle = 'tableByProvider'
const containerStyle = 'grayFont'
const searchBoxStyle = 'searchBoxStyle'
const columnFields = [
  { id:'name', label: 'Name', width: 5 },
  { id:'tel', label: 'Teléfono', width: 10 },
  { id:'buttons', label: 'Action', width: 15 }
]
const dataSets = [
  { buttons: ButtonTest(), tel: '1', name: 's', },
  { name: 'Data', tel: '2', buttons: ButtonTest() },
  { name: 'E', tel: '3', buttons: ButtonTest() },
  { name: 'Fta', tel: '4', buttons: ButtonTest() },
  { name: 'A', tel: '5', buttons: ButtonTest() },
  { name: 'B', tel: '6', buttons: ButtonTest() },
  { name: 'C', tel: '7', buttons: ButtonTest() },
  { name: 'A', tel: '8', buttons: ButtonTest() },
  { name: 'B', tel: '9', buttons: ButtonTest() },
  { name: 'C', tel: '10', buttons: ButtonTest() },
  { buttons: ButtonTest(), tel: '1', name: 's', },
  { name: 'Data', tel: '2', buttons: ButtonTest() },
  { name: 'E', tel: '3', buttons: ButtonTest() },
  { name: 'Fta', tel: '4', buttons: ButtonTest() },
  { name: 'A', tel: '5', buttons: ButtonTest() },
  { name: 'B', tel: '6', buttons: ButtonTest() },
  { name: 'C', tel: '7', buttons: ButtonTest() },
  { name: 'A', tel: '8', buttons: ButtonTest() },
  { name: 'B', tel: '9', buttons: ButtonTest() },
  { name: 'C', tel: '10', buttons: ButtonTest() },
  { buttons: ButtonTest(), tel: '1', name: 's', },
  { name: 'Data', tel: '2', buttons: ButtonTest() },
  { name: 'E', tel: '3', buttons: ButtonTest() },
  { name: 'Fta', tel: '4', buttons: ButtonTest() },
  { name: 'A', tel: '5', buttons: ButtonTest() },
  { name: 'B', tel: '6', buttons: ButtonTest() },
  { name: 'C', tel: '7', buttons: ButtonTest() },
  { name: 'A', tel: '8', buttons: ButtonTest() },
  { name: 'B', tel: '9', buttons: ButtonTest() },
  { name: 'C', tel: '10', buttons: ButtonTest() },
  { buttons: ButtonTest(), tel: '1', name: 's', },
  { name: 'Data', tel: '2', buttons: ButtonTest() },
  { name: 'E', tel: '3', buttons: ButtonTest() },
  { name: 'Fta', tel: '4', buttons: ButtonTest() },
  { name: 'A', tel: '5', buttons: ButtonTest() },
  { name: 'B', tel: '6', buttons: ButtonTest() },
  { name: 'C', tel: '7', buttons: ButtonTest() },
  { name: 'A', tel: '8', buttons: ButtonTest() },
  { name: 'B', tel: '9', buttons: ButtonTest() },
  { name: 'C', tel: '10', buttons: ButtonTest() },
  { buttons: ButtonTest(), tel: '1', name: 's', },
  { name: 'Data', tel: '2', buttons: ButtonTest() },
  { name: 'E', tel: '3', buttons: ButtonTest() },
  { name: 'Fta', tel: '4', buttons: ButtonTest() },
  { name: 'A', tel: '5', buttons: ButtonTest() },
  { name: 'B', tel: '6', buttons: ButtonTest() },
  { name: 'C', tel: '7', buttons: ButtonTest() },
  { name: 'A', tel: '8', buttons: ButtonTest() },
  { name: 'B', tel: '9', buttons: ButtonTest() },
  { name: 'C', tel: '10', buttons: ButtonTest() },        
]

class App extends Component {  
  render() {

    return (
        <ComponentsContainer
          tractNumber={5}
          maxItemsPag={5}
          columnFields={columnFields}
          dataSets={dataSets}
          searchBoxPlaceHolder={searchBoxPlaceHolder}
          containerStyle={containerStyle}
          selectedItemPagStyle={selectedPagStyle}
          paginationStyle={paginationStyle}
          tableStyle={tableStyle}
          searchBoxStyle={searchBoxStyle}
        />
    )
  }
}

export default App